# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from django.core import management
from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.test.utils import override_settings

_CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
_LDAP_FAKE_DATA_LOCATION = os.path.join(_CURRENT_DIRECTORY, 'test_ldap_data.json')


@override_settings(
    LDAP_SYNC_URI='ldap://localhost/ou=Users,dc=example,dc=org',
    LDAP_TESTING_FAKE_DATA=_LDAP_FAKE_DATA_LOCATION
)
class LdapTestCase(TestCase):
    def setUp(self):
        self.USER_MODEL = get_user_model()
        self.client = Client(REMOTE_USER='test')
    
    def test_sync_attributes(self):
        """
        Checks if the attributes of the user "test" are automatically
        synced from LDAP when creating the user.
        """
        user = self.USER_MODEL.objects.create(username='test')
        self.assertEquals(user.first_name, 'Test')
        self.assertEquals(user.last_name,  'User')
        self.assertEquals(user.email, 'test@example.org')
        
        user = self.USER_MODEL.objects.get(username='test')
        self.assertEquals(user.first_name, 'Test')
        self.assertEquals(user.last_name,  'User')
        self.assertEquals(user.email, 'test@example.org')

    def test_autocreate(self):
        response = self.client.get('/admin/')
        self.assertEquals(response.status_code, 302)
        
        user = self.USER_MODEL.objects.get(username='test')
        self.assertEquals(user.first_name, 'Test')
        self.assertEquals(user.last_name,  'User')
        self.assertNotEqual(user.email, '')
    
    def test_management_command(self):
        self.USER_MODEL.objects.create(username='test')
        
        # Clear user attributes for this test
        self.USER_MODEL.objects.all().update(first_name='', last_name='', email='')
        user = self.USER_MODEL.objects.get(username='test')
        self.assertEquals(user.first_name, '')
        self.assertEquals(user.last_name,  '')
        self.assertEquals(user.email, '')
    
        # Now run management command to test if sync is working
        management.call_command('ldap_sync')
        user = self.USER_MODEL.objects.get(username='test')
        self.assertEquals(user.first_name, 'Test')
        self.assertEquals(user.last_name,  'User')
        self.assertNotEqual(user.email, '')

    def test_management_command_is_active(self):
        """
        Tests if the `is_active` flag is synced correctly for active/inactive
        LDAP users.
        """
        with self.settings(LDAP_SYNC_IS_ACTIVE=True):
            self.USER_MODEL.objects.create(username='test',  is_active=True)
            self.USER_MODEL.objects.create(username='alice', is_active=False)
            self.USER_MODEL.objects.create(username='not_in_ldap', is_active=True)
            
            # Clear user attributes for this test
            self.USER_MODEL.objects.all().update(first_name='', last_name='', email='')
        
            # Now run management command to test syncing of `is_active`
            management.call_command('ldap_sync')
            user = self.USER_MODEL.objects.get(username='test')
            self.assertTrue(user.is_active)
            user = self.USER_MODEL.objects.get(username='alice')
            self.assertTrue(user.is_active)
            user = self.USER_MODEL.objects.get(username='not_in_ldap')
            self.assertFalse(user.is_active)

    def test_management_command_without_is_active_sync(self):
        """
        Check if the setting `LDAP_SYNC_IS_ACTIVE` is honored by the
        `ldap_sync`sync` management command.
        """
        with self.settings(LDAP_SYNC_IS_ACTIVE=False):
            self.USER_MODEL.objects.create(username='test',  is_active=True)
            self.USER_MODEL.objects.create(username='alice', is_active=False)
            self.USER_MODEL.objects.create(username='not_in_ldap', is_active=True)
            
            # Clear user attributes for this test
            self.USER_MODEL.objects.all().update(first_name='', last_name='', email='')
        
            # Now run management command, it should not touch the `is_active` flags
            # as `LDAP_SYNC_IS_ACTIVE` is disabled.
            management.call_command('ldap_sync')
            user = self.USER_MODEL.objects.get(username='test')
            self.assertTrue(user.is_active)
            user = self.USER_MODEL.objects.get(username='alice')
            self.assertFalse(user.is_active)
            user = self.USER_MODEL.objects.get(username='not_in_ldap')
            self.assertTrue(user.is_active)

    def test_management_command_exclude_arguments(self):
        with self.settings(LDAP_SYNC_IS_ACTIVE=True):
            self.USER_MODEL.objects.create(username='test',  is_active=False)
            self.USER_MODEL.objects.create(username='alice', is_active=False)
            self.USER_MODEL.objects.create(username='api-not_in_ldap', is_active=True)
            
            # Clear user attributes for this test
            self.USER_MODEL.objects.all().update(first_name='', last_name='', email='')
        
            stdout = StringIO()
        
            management.call_command('ldap_sync', exclude=['alice'], verbosity=3, stdout=stdout)
            user = self.USER_MODEL.objects.get(username='test')
            self.assertTrue(user.is_active)
            user = self.USER_MODEL.objects.get(username='alice')
            self.assertFalse(user.is_active)
            user = self.USER_MODEL.objects.get(username='api-not_in_ldap')
            self.assertTrue(user.is_active)
            
            # Check if output matches
            output_lines = set(stdout.getvalue().splitlines())
            self.assertIn("Ignoring api-not_in_ldap", output_lines)
            self.assertIn("Ignoring alice", output_lines)
    
    def test_invalid_user(self):
        self.USER_MODEL.objects.create(username='unknown')
        user = self.USER_MODEL.objects.get(username='unknown')
        self.assertEquals(user.first_name, '')
        self.assertEquals(user.last_name, '')
        self.assertEquals(user.email, '')

    def test_attribute_max_lenth(self):
        """
        Tests if LDAP attributes, which are longer than the `max_length` of
        the equivalent Django field, are cut off properly. 
        """
        last_name_field = self.USER_MODEL._meta.get_field('last_name')
        if last_name_field.max_length >= 200:
            self.skipTest("The `max_length` of the `last_name` field is too large for this test.")
        
        user = self.USER_MODEL.objects.create(username='long')
        self.assertEquals(user.first_name, 'Long Name')
        self.assertEquals(len(user.last_name), last_name_field.max_length)
