#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess

p = subprocess.Popen(['coverage', 'report'], stdout=subprocess.PIPE)
out, err = p.communicate()
out = out.decode('utf-8') 

for line in out.splitlines():
    print(line)
    if line.startswith('TOTAL'):
        total = line.split()
        print("pycoverage: %s" %total[3])

