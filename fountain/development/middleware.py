import logging
import platform
from django.conf import settings
from django.contrib.auth.middleware import RemoteUserMiddleware

class LocalUserMiddleware(RemoteUserMiddleware):
    """
    This middleware can be used for developing Django projects in
    environments where RemoteUserMiddleware is used in production
    environments and a 1:1 mapping between local user accounts and
    remote users exist.
    
    :attention: Never use this middleware in production environments!
    """
    if platform.system() == 'Windows':
        header = 'USERNAME'
    else:
        header = 'LOGNAME'

    def __init__(self, *args, **kwargs):
        super(LocalUserMiddleware, self).__init__(*args, **kwargs)
        # Prevent accidental usage in production environments
        if settings.DEBUG == False:
            logger = logging.getLogger(__name__)
            logger.warning('The LocalUserMiddleware should never be used in production environments!')
