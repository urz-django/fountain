import logging
import ssl
import ldap3
from ldap3 import Server, Connection, SYNC, OFFLINE_SLAPD_2_4, MOCK_SYNC
from ldap3.core.exceptions import LDAPException
from ldap3.core.tls import Tls
from ldap3.utils.uri import parse_uri
from ldap3.utils.conv import escape_filter_chars 
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.functional import cached_property

DEFAULT_LDAP_TIMEOUT = 3
DEFAULT_LDAP_SYNC_URI = 'ldaps://ldap.tu-chemnitz.de/ou=Users,dc=tu-chemnitz,dc=de'

DEFAULT_LDAP_SYNC_USER_ATTRIBUTES = {
    'givenName': 'first_name',
    'sn': 'last_name',
    'mail': 'email',
}

logger = logging.getLogger(__name__)


class Ldap(object):
    def __init__(self):
        self.LDAP_SYNC_URI = getattr(settings, 'LDAP_SYNC_URI', DEFAULT_LDAP_SYNC_URI) 
        self.LDAP_PARAMS   = parse_uri(self.LDAP_SYNC_URI)
        self.LDAP_SYNC_BASE_USER = getattr(settings, 'LDAP_SYNC_BASE_USER', None)
        self.LDAP_SYNC_BASE_PASS = getattr(settings, 'LDAP_SYNC_BASE_PASS', None)
        self.LDAP_SYNC_USER_ATTRIBUTES = getattr(settings, 'LDAP_SYNC_USER_ATTRIBUTES', DEFAULT_LDAP_SYNC_USER_ATTRIBUTES)
        self.LDAP_CA_CERT = getattr(settings, 'LDAP_CA_CERT', None)
        self.LDAP_TIMEOUT = getattr(settings, 'LDAP_TIMEOUT', DEFAULT_LDAP_TIMEOUT)
        
        # For testing with fake LDAP attributes we can use the following settings
        self._LDAP_TESTING_FAKE_DATA   = getattr(settings, 'LDAP_TESTING_FAKE_DATA', None)
        self._LDAP_TESTING_FAKE_SCHEMA = getattr(settings, 'LDAP_TESTING_FAKE_SCHEMA', OFFLINE_SLAPD_2_4)
        
        # Get the `max_length` of synced attributes from the installed User model.
        # This is used in `get_attributes` to cut off values which are too long for
        # the database schema.
        User = get_user_model()
        self.USER_MODEL_ATTRS_MAX_LENGTH = {}
        for field_name in self.LDAP_SYNC_USER_ATTRIBUTES.values():
            field = User._meta.get_field(field_name)
            self.USER_MODEL_ATTRS_MAX_LENGTH[field_name] = field.max_length
    
    @cached_property
    def connection(self):
        # This can be used for testing in order to load random test data
        if self._LDAP_TESTING_FAKE_DATA:
            fake_server = Server('fake_testing_server', get_info=self._LDAP_TESTING_FAKE_SCHEMA)
            fake_conn   = Connection(fake_server, user='', password='', client_strategy=MOCK_SYNC)
            fake_conn.strategy.entries_from_json(self._LDAP_TESTING_FAKE_DATA)
            fake_conn.bind()
            return fake_conn
        else:
            server_kwargs = {
                'host': self.LDAP_PARAMS['host'],
                'use_ssl': False,
                'port': self.LDAP_PARAMS['port'],
                'connect_timeout': self.LDAP_TIMEOUT,
            }
            if self.LDAP_PARAMS['ssl']:
                server_kwargs['use_ssl'] = True
                server_kwargs['tls'] = Tls(ca_certs_file=self.LDAP_CA_CERT, validate=ssl.CERT_REQUIRED)
                
            s = Server(**server_kwargs)
            connection_kwargs = {
                'server': s,
                'auto_bind': True,
                'user': self.LDAP_SYNC_BASE_USER,
                'password': self.LDAP_SYNC_BASE_PASS,
                'client_strategy': SYNC,
            }
            try:
                return Connection(**connection_kwargs)
            except LDAPException:
                logger.warning("LDAP connection failed, LDAP updates will not be available.")
                return None

    def get_attributes(self, username):
        """
        :attention: This method is not part of the public API. Do not use it.
        """
        model_attrs = {}    
        ldap_user = self.get_user(username, self.LDAP_SYNC_USER_ATTRIBUTES.keys())
        
        for attr in self.LDAP_SYNC_USER_ATTRIBUTES:
            if attr in ldap_user and ldap_user[attr]:
                field_name = self.LDAP_SYNC_USER_ATTRIBUTES[attr]
                ldap_value = ldap_user[attr][0]
                # Limit the LDAP value to the `max_length` of the field. Otherwise
                # we run into validation errors.
                model_attrs[field_name] = ldap_value[0:self.USER_MODEL_ATTRS_MAX_LENGTH[field_name]]
        return model_attrs

    def get_user(self, username, attributes=ldap3.ALL_ATTRIBUTES):
        """
        Returns the specified user from LDAP, without doing any conversion.
        
        :rtype: dict
        """
        cleaned_username = escape_filter_chars(username)
        search_kwargs = {
            'search_base': self.LDAP_PARAMS['base'],
            'search_filter': '(uid=%s)' %cleaned_username,
            'attributes': attributes,
        }
        
        conn = self.connection
        if not conn:
            return {}
        try:
            result = conn.search(**search_kwargs)
        except LDAPException:
            # Try one more time before raising the exception
            # @TODO: Catch exception in User.pre_save()
            try:
                conn.unbind()
            except:
                pass
            conn.bind()
            result = conn.search(**search_kwargs)
        
        if not result:
            return {}
        else:
            return conn.response[0]['attributes']
