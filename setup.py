#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='django-fountain',
    version='0.4',
    description='Synchronizes the attributes of existing Django users from LDAP.',
    author='Daniel Klaffenbach',
    author_email='daniel.klaffenbach@hrz.tu-chemnitz.de',
    url='https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain/',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'django>=3.2',
        'ldap3>=2.2.0',
    ],
)
