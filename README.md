django-fountain
===============

[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain/badges/master/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain/-/commits/master)
[![coverage report](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain/badges/master/coverage.svg)](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain/-/commits/master)


Django-fountain is yet another LDAP user sync app. The main difference compared
to existing solutions is that it does not sync the entire directory. It only
synchronizes the attributes of existing Django users.

Installation
------------

1. Install `django-fountain` through `pip`
2. Add `fountain` to `INSTALLED_APPS`

Configuration
-------------

The following settings may be configured:

- `LDAP_SYNC_URI`: This needs to be a LDAP **URI**, such as `ldaps://ldap.example.org/ou=Users,dc=example,dc=org`
- `LDAP_SYNC_USER_ATTRIBUTES`: A dictionary mapping LDAP attributes to Django model attributes
- `LDAP_SYNC_IS_ACTIVE`: Normally a user account is disabled when the user was not found in LDAP. Set this to `False` if you do not want this behavior.

Default values are used if the settings are not present. They may or may not work.

Regular attribute sync
----------------------

Normally attributes are only synced when a user is being created. To update the
attributes of all users regularly the following management command may be used:

```bash
./manage.py ldap_sync
```

By default the `ldap_sync` command will disable all users which cannot be found
in the LDAP directory. You can either prevent syncing of specified users
(see `--exclude` and/or `--exclude-regex` command line switches) or you can
prevent touching the `is_active` flag for all users by changing the setting
`LDAP_SYNC_IS_ACTIVE` to `False`.
